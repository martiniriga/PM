<?php

require 'Slim/Slim.php';

$app = new Slim();

require_once 'users.php';

$app->get('/setup', 'setup');
$app->post('/tasks', 'addtask');
$app->put('/tasks/:id', 'updatetask');
$app->delete('/tasks/:id', 'deletetask');

$app->run();

function getConnection() {
	$dbhost="127.0.0.1";
	$dbuser="root";
	$dbpass="";
	$dbname="masterp1_journal";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}


function setup() {
	$data ="";
	$sql = "select u.fullnames,u.id,tt.task,count(t.id) as count FROM task t INNER JOIN user u ON t.user_id =u.id INNER JOIN task_type tt ON t.type_id = tt.id WHERE t.status=3 group by t.type_id ,u.id";// t WHERE t.type_id IN(1,2) ";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);
		$tskUserCt = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		// $data = '{"taskUserCount": ' . json_encode($tasks) . '}';
		$data['tskuserct'] = $tskUserCt;
	} catch(PDOException $e) {
		$data['error'] = $e->getMessage();//{"error":{"text":SQLSTATE[42S22]: Column not found: 1054 Unknown column 'j.fed' in 'field list'}}
	}
	$sql = "SELECT t.*, u.fullnames FROM task t INNER JOIN user u ON t.user_id = u.id";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);
		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$data['tasks'] = $tasks;

		// echo '{"tsk": ' . json_encode($tasks) . '}';
	} catch(PDOException $e) {
		$data['error'] = $e->getMessage();
	}

	echo(json_encode($data));

}

function addtask() {
	$request = Slim::getInstance()->request();

	$task = json_decode($request->getBody());

	$sql = "INSERT INTO task (type_id, user_id, task, status, created) VALUES (:type_id, :user_id, :task, :status, :created)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("type_id", $task->type_id);
		$stmt->bindParam("user_id", $task->user_id);
		$stmt->bindParam("task", $task->task);
		$stmt->bindValue("status", 1);
		$stmt->bindValue("created", date('Y-m-d'));
		$stmt->execute();

		$task->id = $db->lastInsertId();
		$db = null;
		$tasktype ="";
		switch ($task->type_id) {
			case '1':
			$tasktype ="Frontend Task";
			break;
			case '2':
			$tasktype ="Backend Task";
			break;
			case '3':
			$tasktype ="Design Task";
			break;
			case '4':
			$tasktype ="Support Task";
			break;
			default:
			$tasktype ="Other Task";
			break;
		}
		echo json_encode(array("success"=>true,"text" =>"Added a ".$tasktype.": ".substr($task->task,0,40).".. successfully"));
	} catch(PDOException $e) {
		echo json_encode(array("success"=>false,"text" =>$e->getMessage()));
	}
}

function updatetask($id) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$task = json_decode($body);

	if (isset($task->task)){
		$sql = "UPDATE task SET task=:task, type_id=:type_id, user_id=:user_id, created=:created WHERE id=:id";
	}else{
		$sql = "UPDATE task SET status=:status,user_id=:user_id WHERE id=:id";
		if ($task->status == 3)
		$sql = "UPDATE task SET status=:status,user_id=:user_id,completed=:completed WHERE id=:id";
	}

	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $task->user_id);
		$stmt->bindValue("id", $id);

		if (isset($task->task)){
			$stmt->bindParam("task", $task->task);
			$stmt->bindParam("type_id", $task->type_id);
			$stmt->bindValue("created", date('Y-m-d'));
		}else{
			$stmt->bindParam("status", $task->status);
			if ($task->status == 3)
			$stmt->bindValue("completed", date("Y-m-d"));
		}

		$stmt->execute();
		$db = null;

		if(isset($task->task)){
			echo json_encode(array("success"=>true,"text" =>"Task:".substr($task->task,0,40).".. updated successfully"));
		}else{
			if ($task->status == 3)
			echo json_encode(array("success"=>true,"text" =>"You have Added the task to your completed tasks"));
			else
			echo json_encode(array("success"=>true,"text" =>"You have Added the task to ongoing tasks"));
		}

	} catch(PDOException $e) {
		echo json_encode(array("success"=>false,"text" =>$e->getMessage()));
	}
}

function deletetask($id)
{
	$sql = "DELETE FROM task WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo json_encode(array("success"=>true,"text" =>"Deleted task successfully"));
	} catch(PDOException $e) {
		echo json_encode(array("success"=>false,"text" =>$e->getMessage()));
	}
}

?>
