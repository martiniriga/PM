<?php

$app->get('/login/:email', 'login');

function login($email) {
  $sql = "SELECT * FROM user WHERE email=:email";
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("email", $email);
    $stmt->execute();
    $user = $stmt->fetchObject();
    $ct = $stmt->rowCount();
    $db = null;
    if ($ct>0){
      echo json_encode($user);
    } else
      echo json_encode(array("error"=>"Invalid email address"));

  } catch(PDOException $e) {
    echo json_encode(array("error"=>$e->getMessage()));
  }
}



 ?>
