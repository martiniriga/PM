// The root URL for the RESTful services
var rootURL = "http://localhost/mpf/api/";
// Task arrays
var tasks,startedTasks,ongoingTasks =[];
var logged_in = false;
// task types
var frontendTask = 1;
var backendTask = 2;
var designTask = 3;
var supportTask= 4;
var otherTask= 5;
// Task Statuses
var started = 1;
var ongoing = 2;
var completed =3;
var currenttask;


//Check if logged In
isloggedIn();


$('#loginmodal form').validate({
	rules: {
		txtemail: {
			required: true,
			email: true
		}
	},
	messages: {
		txtemail: {
			required: "Please enter your email address",
			email: "That email address appears to be wrong"
		}
	},
	submitHandler: function (form) {
		var	url =	rootURL+ 'login/'+ $('#txtemail').val();
		$.ajax({
			type: 'GET',
			url: 	rootURL+ 'login/'+ $('#txtemail').val(),
			dataType: "json",
			success: function(data, textStatus, jqXHR){
				if(data.error){
					toastr.error(data.error);
				}else{
					$('#loginmodal').modal('hide');
					toastr.success(data.fullnames + ' logged In');
					$.cookie('user_id',data.id);
					$.cookie('fullnames',data.fullnames);
					$('.name').text($.cookie('fullnames'));
					setupUI();
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				toastr.error(errorThrown);
			}
		});
		return false; // required to block normal submit since you used ajax
	}
});

//open add a task modal
$('.bx').click(function() {
	$('.modal-title').text($(this).attr('title'));
	$('#task_type').val($(this).attr('id'));
	var bgclass = $('#editTaskModal .modal-content').attr('class').split(' ');
	var retainclass = bgclass[0];
	var dumpclass = bgclass[1].split('-');
	retainclass += ' '+ dumpclass[0] + '-' + $(this).attr('id');
	$('#addTaskModal .modal-dialog>div').attr('class',retainclass);
	$('#addTaskModal').modal('show', {backdrop: 'static'});
	addtask();
	return false;
});

//open edit a task modal
$('#tasks>.row,#ongoing>.row').on('click','.fa-edit', function() {
	var taskid = $(this).parent().parent().attr('id');
	taskid = taskid.split('-');
	taskid = taskid[1];
	var tasktypeid = $(this).parent().parent().attr('class').split(' ');
	tasktypeid = tasktypeid[1];
	tasktypeid = tasktypeid.split('-');
	tasktypeid = tasktypeid[1];
	$('#edittaskid').val(taskid);
	$('#txtedittask').val($(this).parent().next('span').text());
	$('#editTskType').val(tasktypeid);
	var bgclass = $('#editTaskModal .modal-content').attr('class').split(' ');
	var retainclass = bgclass[0];
	var dumpclass = bgclass[1].split('-');
	retainclass += ' '+ dumpclass[0] + '-' + tasktypeid;
	$('#editTaskModal .modal-dialog>div').attr('class',retainclass);
	$('#editTaskModal').modal('show', {backdrop: 'static'});
	edittask();
	return false;
});

//open edit a task modal
$('#tasks>.row,#ongoing>.row').on('click','.fa-times', function() {
	var row = $(this).parent().parent().parent();
	var taskid = row.attr('id');
	taskid = taskid.split('-');
	taskid = taskid[1];
	var tasktypeid = row.attr('class').split(' ');
	tasktypeid = tasktypeid[1];
	tasktypeid = tasktypeid.split('-');
	tasktypeid = tasktypeid[1];
	$('#deletetaskid').val(taskid);
	$('#txtdeletetask').val($(this).parent().parent().next('span').text());
	var bgclass = $('#deleteTaskModal .modal-content').attr('class').split(' ');
	var retainclass = bgclass[0];
	var dumpclass = bgclass[1].split('-');
	retainclass += ' '+ dumpclass[0] + '-' + tasktypeid;
	$('#deleteTaskModal .modal-dialog>div').attr('class',retainclass);
	$('#deleteTaskModal .modal-title').text('Delete Task');
	$('#deleteTaskModal').modal('show', {backdrop: 'static'});
	deletetask();
	return false;
});


//logout
$('#logout').click(function() {
	$.removeCookie('fullnames');
	$.removeCookie('upn');
	if($.removeCookie('user_id')){
		toastr.success('Logged Out Successfully');
		location.reload();
	}else {
		toastr.warning('Could not Log Out.User was not logged in');
	}


});


//check if loggedin
function isloggedIn(){
	if (!$.cookie('user_id')){
		$('#loginmodal').modal({backdrop: 'static',keyboard: false});
	}
	else {
		$('.name').html($.cookie('fullnames'));
		setupUI();
	}
}

// Load all users,tasks lists
function setupUI() {
	$.ajax({
		type: 'GET',
		url: rootURL + 'setup',
		dataType: "json",
		success: renderList
	});
}

//clear modals
function clearmodal() {
	$('textarea,select ').val('');
	$('.modal').modal('hide');
}

//add task
function addtask() {
	$('#addTaskModal form').validate({
		rules: {
			txtnewtask: {
				required: true,
				minlength: 10
			}
		},
		messages: {
			txtnewtask: {
				required: "Please enter a task",
				minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
			}
		},
		submitHandler: function (form) {
			var data = JSON.stringify({
				"type_id": $('#task_type').val(),
				"user_id": $.cookie('user_id'),
				"task": $('#txtnewtask').val()
			});

			$.ajax({
				type: 'POST',
				contentType: 'application/json',
				url: 	rootURL+ 'tasks',
				dataType: "json",
				data: data,
				success: function(data, textStatus, jqXHR){
					if (data.success) {
						setupUI();
						clearmodal();
						toastr.success(data.text);
					} else {
						toastr.warning(data.text);
					}

				},
				error: function(jqXHR, textStatus, errorThrown){
					toastr.error(textStatus+': '+errorThrown);
				}
			});
			return false;
		}
	});

}

function edittask() {
	$('#editTaskModal form').validate({
		rules: {
			txtedittask: {
				required: true,
				minlength: 10
			}
		},
		messages: {
			txtedittask: {
				required: "Please enter a task",
				minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
			}
		},
		submitHandler: function (form) {
			var data = JSON.stringify({
				"task": $('#txtedittask').val(),
				"type_id": $('#editTskType').val(),
				"user_id": $.cookie('user_id')
			});
			$.ajax({
				type: 'PUT',
				contentType: 'application/json',
				url: rootURL + 'tasks/' + $('#edittaskid').val(),
				dataType: "json",
				data: data,
				success: function(data, textStatus, jqXHR){
					if (data.success) {
						setupUI();
						clearmodal();
						// ('textarea').val('');
						// $('#editTaskModal').modal('hide');
						toastr.success(data.text);
					} else {
						toastr.warning(data.text);
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					toastr.error(textStatus);
				}
			});
			return false;
		}
	});

}

function updatetask(taskid,status) {
	var data = JSON.stringify({
		"status": status,
		"user_id": $.cookie('user_id')
	});
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + 'tasks/' + taskid,
		dataType: "json",
		data: data,
		success: function(data, textStatus, jqXHR){
			if (data.success) {
				setupUI();
				toastr.success(data.text);
			} else {
				toastr.warning(data.text);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			toastr.error(textStatus);
		}
	});
}

//delete task
function deletetask() {
	// $('document').on('click','#deleteTaskModal .btn-success',function(e){
		$('#deleteTaskModal .btn-success').click(function(e){
		e.preventDefault();
		$.ajax({
			type: 'DELETE',
			url: 	rootURL+ 'tasks/'+ $('#deletetaskid').val(),
			dataType: "json",
			success: function(data, textStatus, jqXHR){
				if (data.success) {
					setupUI();
					clearmodal();
					toastr.success(data.text);
				} else {
					toastr.warning(data.text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				toastr.error(textStatus+': '+errorThrown);
			}
		});
		return false;
	});

}

// render view
function renderList(data) {

	if(data.hasOwnProperty('error'))
	console.log(data.error);

	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	tasks = data == null ? [] : (data.tasks instanceof Array ? data.tasks : [data.tasks]);
	var taskct = data == null ? [] : (data.tskuserct instanceof Array ? data.tskuserct : [data.tskuserct]);

	ongoingTasks = filterTasksByProgress(ongoing);
	startedTasks = filterTasksByProgress(started);
	completedTasks = filterTasksByProgress(completed);

	var html ="";
	for (var i = 0; i < startedTasks.length; i++) {
		html +=	'<div draggable="true" ondragstart="dragStart(event)" data-toggle="context" title="Created by '
		+ startedTasks[i].fullnames+ '" id="stsk-'
		+ startedTasks[i].id + '" class="col-md-4 bg-'
		+ startedTasks[i].type_id +'" ><div class="row"><i class="fa fa-edit"></i><span class="pull-right"><i class="fa fa-times"></i></span></div><span>'
		+startedTasks[i].task+'</span></div>';
	}
	$('#tasks>.row').html(html).fadeIn(3000);
	//reset html
	html= "";
	for (var i = 0; i < ongoingTasks.length; i++) {
		html +=	'<div draggable="true" ondragstart="dragStart(event)" class="col-md-4 bg-'
		+ ongoingTasks[i].type_id +'" title="Assigned to '
		+ ongoingTasks[i].fullnames+ '" id="otsk-'
		+ ongoingTasks[i].id + '" ><div class="row"><i class="fa fa-edit"></i><span class="pull-right"><i class="fa fa-times"></i></span></div><span>'
		+ ongoingTasks[i].task+'</span></div>';
	}
	$('#ongoing>.row').html(html).fadeIn(3000);

	html= "";
	for (var i = 0; i < taskct.length; i++) {
		var firstname = taskct[i].fullnames.split(' ');
		firstname =firstname[0];
		html +=	'<div class="col-xs-12" id="user-' + taskct[i].id + '" ><span>'+firstname+' - '+taskct[i].task+'</span><h1 class="text-center">'+taskct[i].count+'</h1></div>';
	}
	$('#drp-completed').html(html).fadeIn(3000);

}


function filterTasksByProgress(status){
	var query = 'WHERE status = "' + status + '"';
	var sql =  'SELECT * from ? ' + query;
	var results = alasql(sql,[tasks]);
	return results;
}

//dragevents
function dragStart(ev) {
	ev.dataTransfer.effectAllowed='move';
	ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));
	return true;
}

function dragEnter(ev) {
	event.preventDefault();
	return true;
}

function dragOver(ev) {
	return false;
}

function dragDrop(ev) {

	var srcid = ev.dataTransfer.getData("Text");
	// ev.target.appendChild(document.getElementById(srcid));
	this.appendChild(document.getElementById(srcid));
	ev.stopPropagation();
	srcid = srcid.split('-');
	updatetask(srcid[1],ongoing);
	return false;
}
document.getElementById("drp-ongoing").addEventListener("drop", dragDrop, false);

function drop(ev) {
	var srcid = ev.dataTransfer.getData("text");
	this.appendChild(document.getElementById(srcid));
	$('#'+srcid).fadeOut(1000);
	srcid = srcid.split('-');//split to get taskid 0:otsk,1:1
	updatetask(srcid[1],completed);
}
document.getElementById("drp-completed").addEventListener("drop", drop, false);
